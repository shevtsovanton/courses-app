import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @Output() makeSearchQuery: EventEmitter<string>;
  searchQuery = '';

  constructor() {
    this.makeSearchQuery = new EventEmitter();
  }

  search(event) {
    if (event.type === 'click' || event.key === 'Enter') {
      this.makeSearchQuery.emit(this.searchQuery);
      this.searchQuery = '';
    }
  }
}
