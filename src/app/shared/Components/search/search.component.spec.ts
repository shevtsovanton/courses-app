import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { SearchComponent } from './search.component';


describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let hostElement: HTMLElement;
  let searchInput: HTMLInputElement;
  let searchButton: HTMLElement;
  let eventMock: {};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent],
      imports: [
        FormsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    hostElement = fixture.nativeElement;
    searchInput = hostElement.querySelector('input');
    searchButton = hostElement.querySelector('i');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should raise  search makeSearchQuery on "enter" key', () => {
    component.searchQuery = 'test';
    component.makeSearchQuery.subscribe(inputValue => {
      expect(inputValue).toBe('test');
    });
    eventMock = {key: 'Enter'};
    component.search(eventMock);
  });

  it('should clear input field', () => {
    component.searchQuery = 'test';
    eventMock = {type: 'click'};
    component.search(eventMock);
    expect(component.searchQuery).toBe('');
  });

  it('should not clear input field', () => {
    component.searchQuery = 'test';
    eventMock = {type: 'Escape'};
    component.search(eventMock);
    expect(component.searchQuery).toBe('test');
  });
});
