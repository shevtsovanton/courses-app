import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from 'src/app/features/login/services/authorization.service';
import { User } from 'src/app/features/login/classes/user.class';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isAuthenticated: Observable<boolean>;
  user: User;

  constructor(private authService: AuthorizationService) {}

  ngOnInit() {
    this.user = {id: 1, firstName: 'Tom', lastName: 'Smith'};
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  logout() {
    this.authService.logout();
    this.user = null;
  }
}
