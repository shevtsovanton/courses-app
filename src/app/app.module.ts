import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeUa from '@angular/common/locales/ru-UA';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoursesListModule } from './features/courses-list/courses-list.module';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './features/login/login.module';

registerLocaleData(localeUa, 'ru-UA');

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    CoursesListModule,
    FormsModule,
    AppRoutingModule,
    LoginModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'ru-UA'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
