import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VideoCoursesPageComponent } from './features/courses-list/containers/video-courses-page/video-courses-page.component';

const routes: Routes = [
  { path: '', component: VideoCoursesPageComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
