export class CoursesListItem implements CoursesListItem {
    /* istanbul ignore next */
    constructor(
        public id = null,
        public title = '',
        public creationDate = null,
        public duration = null,
        public description = '',
        public topRated = false,
        public imagePath = '',
    ) {}
}


