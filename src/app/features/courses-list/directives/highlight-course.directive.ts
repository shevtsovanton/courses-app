import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlightCourse]'
})
export class HighlightCourseDirective implements OnInit {

  @Input('appHighlightCourse') creationDate: Date;
  currentDateMs: number = Date.now();
  creationDateMs: number;
  freshCourseAge: number = 1000 * 60 * 60 * 24 * 14;


  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.creationDateMs = this.creationDate.getTime();
    this.highlight(this.creationDateMs);
  }


  private highlight(creationDateMs: number): void {
    if ((creationDateMs < this.currentDateMs) && (creationDateMs >= this.currentDateMs - this.freshCourseAge)) {
      this.el.nativeElement.style.border = '2px solid #00FF00';
    }
    if (creationDateMs > this.currentDateMs) {
      this.el.nativeElement.style.border = '2px solid #0000FF';
    }
  }
}
