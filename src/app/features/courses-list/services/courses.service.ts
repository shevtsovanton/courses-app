import { Injectable } from '@angular/core';
import { CoursesListItem } from '../classes/courses-list-item.class';

@Injectable()
export class CoursesService {

  coursesList: CoursesListItem[] = [
    {
      id: 1,
      title: 'Angular',
      creationDate: new Date(2019, 6, 1),
      duration: 32,
      description: 'angular course',
      topRated: false,
      imagePath: '../../../../assets/img/1.jpg'
    },
    {
      id: 2,
      title: 'React',
      creationDate: new Date(2016, 6, 1),
      duration: 90,
      description: 'react course',
      topRated: true,
      imagePath: '../../../../assets/img/2.jpg'
    },
    {
      id: 3,
      title: 'Javascript',
      creationDate: new Date(2016, 6, 1),
      duration: 68,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: false,
      imagePath: '../../../../assets/img/3.jpg'
    },
    {
      id: 4,
      title: 'Express.js',
      creationDate: new Date(2016, 6, 1),
      duration: 333,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: true,
      imagePath: '../../../../assets/img/4.jpg'
    },
    {
      id: 5,
      title: 'Docker',
      creationDate: new Date(2016, 6, 1),
      duration: 444,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: false,
      imagePath: '../../../../assets/img/5.jpg'
    },
    {
      id: 6,
      title: 'React',
      creationDate: new Date(2016, 6, 1),
      duration: 10,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: false,
      imagePath: '../../../../assets/img/7.jpg'
    },
    {
      id: 7,
      title: 'Vue.js',
      creationDate: new Date(2016, 6, 1),
      duration: 280,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: false,
      imagePath: '../../../../assets/img/6.jpg'
    },
    {
      id: 8,
      title: 'Title7',
      creationDate: new Date(2016, 6, 1),
      duration: 135,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      topRated: false,
      imagePath: '../../../../assets/img/7.jpg'
    },
  ];

  getList(): CoursesListItem[] {
    return this.coursesList;
  }

  createCourse(course): void {
    this.coursesList = [...this.coursesList, course];
  }

  getCourse(id): CoursesListItem {
    return this.coursesList.filter( course => course.id === id)[0];
  }

  updateCourse(updatedCourse): void {
    this.coursesList.map( (course, index) => {
      if (course.id === updatedCourse.id) {
        this.coursesList = [
          ...this.coursesList.slice(0, index),
          updatedCourse,
          ...this.coursesList.slice(index + 1)
        ];
      }
    });
  }

  remove(courseToRemove): void {
    this.coursesList.map( (course, index) => {
      if (course.id === courseToRemove.id) {
        this.coursesList = [
          ...this.coursesList.slice(0, index),
          ...this.coursesList.slice(index + 1)
        ];
      }
    });
  }
}
