import { TestBed } from '@angular/core/testing';

import { CoursesService } from './courses.service';
import { CoursesListItem } from '../classes/courses-list-item.class';

const coursesListMock: CoursesListItem[] = [
  {
    id: 1,
    title: 'Angular',
    creationDate: new Date(2019, 6, 1),
    duration: 32,
    description: 'angular course',
    topRated: false,
    imagePath: ''
  },
  {
    id: 2,
    title: 'React',
    creationDate: new Date(2016, 6, 1),
    duration: 90,
    description: 'react course',
    topRated: true,
    imagePath: ''
  }
];

const courseMock: CoursesListItem = {
  id: 3,
  title: 'Angular',
  creationDate: new Date(2019, 6, 1),
  duration: 32,
  description: 'angular course',
  topRated: false,
  imagePath: '../../../../assets/img/1.jpg'
};

let service: CoursesService;

describe('CoursesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoursesService]
    });
    service = TestBed.get(CoursesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should createCourse', () => {
    service.coursesList = coursesListMock;
    const coursesQuantity = service.coursesList.length;
    service.createCourse(courseMock);
    expect(service.coursesList.length).toBe(coursesQuantity + 1);
  });

  it('should return course by id', () => {
    service.coursesList = coursesListMock;
    const angularCourse = service.getCourse(1);
    expect(angularCourse.title).toBe('Angular');
  });

  it('should update course ', () => {
    service.coursesList = coursesListMock;
    const updatedCourse = {
      id: 1,
      title: 'UpdatedTitle',
      creationDate: new Date(2019, 6, 1),
      duration: 32,
      description: 'angular course',
      topRated: false,
      imagePath: ''
    };
    service.updateCourse(updatedCourse);
    expect(service.coursesList[0].title).toBe('UpdatedTitle');
  });

  it('should remove course ', () => {
    service.coursesList = coursesListMock;
    const courseToRemove = service.coursesList[0];
    const coursesQuantity = service.coursesList.length;
    service.remove(courseToRemove);
    expect(service.coursesList.length).toBe(coursesQuantity - 1);
  });

  it('should not remove course ', () => {
    service.coursesList = coursesListMock;
    const courseToRemove = {};
    const coursesQuantity = service.coursesList.length;
    service.remove(courseToRemove);
    expect(service.coursesList.length).toBe(coursesQuantity);
  });

});
