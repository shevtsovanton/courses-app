import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { CoursesListItemComponent } from './components/courses-list-item/courses-list-item.component';
import { LoadMoreButtonComponent } from './components/load-more-button/load-more-button.component';
import { VideoCoursesPageComponent } from './containers/video-courses-page/video-courses-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MinutesToHoursPipe } from './pipes/minutes-to-hours.pipe';
import { HighlightCourseDirective } from './directives/highlight-course.directive';
import { CoursesService } from './services/courses.service';

@NgModule({
  declarations: [
    CoursesListComponent,
    CoursesListItemComponent,
    LoadMoreButtonComponent,
    VideoCoursesPageComponent,
    MinutesToHoursPipe,
    HighlightCourseDirective
  ],
  providers: [
    CoursesService,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    VideoCoursesPageComponent,
  ],
})
export class CoursesListModule { }
