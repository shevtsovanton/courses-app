import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesListItemComponent } from './courses-list-item.component';
import { CoursesListItem } from '../../classes/courses-list-item.class';
import { CoursesListModule } from '../../courses-list.module';

const courseMock: CoursesListItem = {
  id: null,
  title: '',
  creationDate: new Date(2019, 6, 1),
  duration: 90,
  description: '',
  topRated: true,
  imagePath: ''
};

const expectedPipedDuration = '1 h 30 min';

describe('CoursesListItemComponent', () => {
  let component: CoursesListItemComponent;
  let fixture: ComponentFixture<CoursesListItemComponent>;
  let hostElement: HTMLElement;
  let deleteButton: HTMLButtonElement;
  let durationElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ CoursesListModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListItemComponent);
    component = fixture.componentInstance;
    hostElement = fixture.nativeElement;
    component.course = courseMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display video duration', () => {
    durationElement = hostElement.querySelector('.course__duration');
    expect(durationElement.textContent).toContain(expectedPipedDuration);
  });

  it('raises the deleteCourse event when clicked', () => {
    component.deleteCourse.subscribe(selectedCourse => expect(selectedCourse).toBe(component.course));
    deleteButton = hostElement.querySelector('.course__button_delete');
    deleteButton.click();
  });
});
