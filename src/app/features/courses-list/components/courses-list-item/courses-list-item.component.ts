import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CoursesListItem } from '../../models/courses-list-item.model';


@Component({
  selector: 'app-courses-list-item',
  templateUrl: './courses-list-item.component.html',
  styleUrls: ['./courses-list-item.component.scss']
})
export class CoursesListItemComponent {

  @Input() course: CoursesListItem;
  @Output() deleteCourse: EventEmitter<CoursesListItem>;

  constructor() {
    this.deleteCourse = new EventEmitter();
  }

  delete() {
    this.deleteCourse.emit(this.course);
  }
}
