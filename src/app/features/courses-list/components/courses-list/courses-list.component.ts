import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CoursesListItem } from '../../models/courses-list-item.model';


@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent {
  @Input() coursesList: CoursesListItem[];
  @Output() deleteCourse: EventEmitter<CoursesListItem>;

  constructor() {
    this.deleteCourse = new EventEmitter();
  }

  delete(course) {
    this.deleteCourse.emit(course);
  }

}
