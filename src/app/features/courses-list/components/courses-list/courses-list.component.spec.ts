import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesListComponent } from './courses-list.component';
import { CoursesListItem } from '../../classes/courses-list-item.class';
import { CoursesListModule } from '../../courses-list.module';

const coursesListMock: CoursesListItem[] = [
  {
    id: null,
    title: '',
    creationDate: new Date(2019, 6, 1),
    duration: null,
    description: '',
    imagePath: '',
    topRated: true
  }
];

describe('CoursesListComponent', () => {
  let component: CoursesListComponent;
  let fixture: ComponentFixture<CoursesListComponent>;
  let hostElement: HTMLElement;
  let appCoursesListItem: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoursesListModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListComponent);
    component = fixture.componentInstance;
    component.coursesList = coursesListMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should raise deleteCourse event', () => {
    spyOn(component.deleteCourse, 'emit');
    hostElement = fixture.nativeElement;
    appCoursesListItem = hostElement.querySelector('.courses__list');
    appCoursesListItem.dispatchEvent(new Event('deleteCourse'));
    expect(component.deleteCourse.emit).toHaveBeenCalled();
  });
});
