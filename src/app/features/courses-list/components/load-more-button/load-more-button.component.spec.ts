import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadMoreButtonComponent } from './load-more-button.component';

describe('LoadMoreButtonComponent', () => {
  let component: LoadMoreButtonComponent;
  let fixture: ComponentFixture<LoadMoreButtonComponent>;
  let hostElement: HTMLElement;
  let loadMoreButton: HTMLButtonElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMoreButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    spyOn(window.console, 'log');
    fixture = TestBed.createComponent(LoadMoreButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log message to console', () => {
    hostElement = fixture.nativeElement;
    loadMoreButton = hostElement.querySelector('button');
    loadMoreButton.click();
    expect(window.console.log).toHaveBeenCalled();
});

});
