import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoCoursesPageComponent } from './video-courses-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoursesListModule } from '../../courses-list.module';
import { CoursesListItem } from '../../classes/courses-list-item.class';

const coursesListMock: CoursesListItem[] = [
  {
    id: 1,
    title: 'React',
    creationDate: new Date(2018, 6, 1),
    duration: 90,
    description: 'angular course',
    topRated: true,
    imagePath: ''
  },
  {
    id: 2,
    title: 'Angular',
    creationDate: new Date(2016, 6, 1),
    duration: 90,
    description: 'react course',
    topRated: true,
    imagePath: ''
  }
];

const filteredCoursesListMock: CoursesListItem[] = [];

describe('VideoCoursesPageComponent', () => {
  let component: VideoCoursesPageComponent;
  let fixture: ComponentFixture<VideoCoursesPageComponent>;
  let hostElement: HTMLElement;
  let appCourseListElement: HTMLElement;
  let appSearchElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoursesListModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoCoursesPageComponent);
    component = fixture.componentInstance;
    component.coursesList = coursesListMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal', () => {
    spyOn(component, 'openModal');
    hostElement = fixture.nativeElement;
    appCourseListElement = hostElement.querySelector('app-courses-list');
    appCourseListElement.dispatchEvent(new Event('deleteCourse'));
    expect(component.openModal).toHaveBeenCalled();
  });

  it('should call search method', () => {
    spyOn(component, 'search');
    hostElement = fixture.nativeElement;
    appSearchElement = hostElement.querySelector('app-search');
    appSearchElement.dispatchEvent(new Event('makeSearchQuery'));
    expect(component.search).toHaveBeenCalled();
  });

  it('should filter results', () => {
    component.coursesList = coursesListMock;
    component.filteredCoursesList = filteredCoursesListMock;
    component.search('Angular');
    expect(component.filteredCoursesList[0].title).toBe('Angular');
  });

  it('should sort courses list', () => {
    component.coursesList = coursesListMock;
    const unsortedCoursesFirstTitle = component.coursesList[0].title;
    const sortedCoursesFirstTitle = component.sortedCoursesList[0].title;
    expect(unsortedCoursesFirstTitle).toBe(sortedCoursesFirstTitle);
  });

  it('should display all courses', () => {
    component.coursesList = coursesListMock;
    component.filteredCoursesList = filteredCoursesListMock;
    component.search(' ');
    expect(component.filteredCoursesList.length).toBe(component.coursesList.length);
  });

});
