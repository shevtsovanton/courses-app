import { Component, OnInit } from '@angular/core';
import { LowerCasePipe } from '@angular/common';

import { CoursesListItem } from '../../classes/courses-list-item.class';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'app-video-courses-page',
  templateUrl: './video-courses-page.component.html',
  styleUrls: ['./video-courses-page.component.scss'],
  providers: [LowerCasePipe]
})

export class VideoCoursesPageComponent implements OnInit {
  coursesList: CoursesListItem[];
  filteredCoursesList: CoursesListItem[];
  isModalOpen: boolean;
  noDataMessage = 'No Data, feel free to add new course';
  mockDescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
  courseToBeDeleted: CoursesListItem;

  constructor(private lowerCase: LowerCasePipe,
              private coursesService: CoursesService,
    ) {}

  ngOnInit() {
    this.coursesList = this.coursesService.getList();
    this.filteredCoursesList = [...this.coursesList];
  }

  private toLowerCase(item: string) {
    return this.lowerCase.transform(item);
  }

  delete(course) {
      this.coursesService.remove(course);
      this.filteredCoursesList = this.coursesService.getList();
  }

  search(searchQuery) {
    const requestedTitle = this.toLowerCase(searchQuery.trim());
    if (!requestedTitle) {
      this.filteredCoursesList = [...this.coursesList];
      return;
    }

    const filteredList = this.coursesList.filter( course => {
      const title = this.toLowerCase(course.title.trim());
      return title.indexOf(requestedTitle) !== -1;
    });

    this.filteredCoursesList = [...filteredList];
  }

  public get sortedCoursesList(): CoursesListItem[] {
    return this.coursesList.sort( (itemA, itemB) => itemB.creationDate - itemA.creationDate);
  }

  openModal(course) {
    this.courseToBeDeleted = course;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  handleModalResponse(event) {
    if (event) {
      this.delete(this.courseToBeDeleted);
    }
    this.closeModal();
  }
}
