import { AuthorizationService } from './authorization.service';
import { User } from '../classes/user.class';

const userMock: User = {
  id: 1,
  firstName: 'Tom',
  lastName: 'Hanks'
};

describe('AuthorizationService', () => {
  let service: AuthorizationService;
  beforeEach(() => {
    service = new AuthorizationService();
    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
  });

  it('should login', () => {
    spyOn(service.isLoginSubject, 'next');
    service.login(userMock);
    expect(service.isLoginSubject.next).toHaveBeenCalled();
  });

  it('should logout', () => {
    spyOn(service.isLoginSubject, 'next');
    service.logout();
    expect(service.isLoginSubject.next).toHaveBeenCalled();
  });

  it('should get user info from local storage', () => {
    service.login(userMock);
    const userInfo = service.getUserInfo();
    expect(userInfo.lastName).toBe('Hanks');
  });
});
