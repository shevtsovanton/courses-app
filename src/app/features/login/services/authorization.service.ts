import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../classes/user.class';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

  isAuthenticated(): Observable<boolean> {
    return this.isLoginSubject.asObservable();
   }

  private hasToken(): boolean {
    return !!localStorage.getItem('loginInfo');
  }

  login(user) {
    const loginInfo = {
      user,
      token: Date.now()
    };
    localStorage.setItem('loginInfo', JSON.stringify(loginInfo));
    this.isLoginSubject.next(this.hasToken());
    console.log(`You are logged in successfully as ${user}`);
  }

  logout() {
    localStorage.removeItem('loginInfo');
    this.isLoginSubject.next(this.hasToken());
    console.log('You are logged out');
  }

  getUserInfo(): User {
    const loginInfo = localStorage.getItem('loginInfo');
    if (loginInfo) {
      console.log('loginInfo', JSON.parse(loginInfo));
      return JSON.parse(loginInfo).user;
    }
  }
}
