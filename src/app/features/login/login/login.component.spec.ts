import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ SharedModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit login and password', () => {
    debugElement = fixture.debugElement;
    const loginInput: HTMLInputElement = debugElement.query(By.css('input[type="text"]')).nativeElement;
    const passwordInput: HTMLInputElement = debugElement.query(By.css('input[type="password"]')).nativeElement;
    loginInput.value = 'Adam';
    passwordInput.value = 'West';
    component.onSubmit(loginInput, passwordInput);
    expect(loginInput.value).toBe('');
    expect(passwordInput.value).toBe('');
  });

});
