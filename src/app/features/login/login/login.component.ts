import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isAuthenticated: Observable<boolean>;
  constructor(private authService: AuthorizationService) {}

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  onSubmit(username: HTMLInputElement, password: HTMLInputElement) {
    if (username.value && password.value) {
      this.authService.login(username.value);
      console.log(`username: ${username.value}, password: ${password.value}`);
      username.value = '';
      password.value = '';
    } else {
      console.log('Please fill in the login and password fields');
    }
  }
}
