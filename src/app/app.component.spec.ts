import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoursesListModule } from './features/courses-list/courses-list.module';
import { LoginModule } from './features/login/login.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        SharedModule,
        CoursesListModule,
        LoginModule
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
